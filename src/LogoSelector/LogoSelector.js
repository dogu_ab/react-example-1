import React from 'react';
import './LogoSelector.css';

const LogoSelector = (props) => {

	const handleChange = (event) => {
		if(event.target.files[0]) {
			props.updateFile({file: event.target.files[0]});

			if(props.stateArray.step === 2)
				props.updateStep({step:3});
		}
		else {
			props.updateFile({file: ''});
			props.updateStep({step:2});
		}
	}

	let previewImage = null;
	if(props.fileArray.file) {
		previewImage = (
			<div>
				<img className="partnerImage" src={URL.createObjectURL(props.fileArray.file)} alt="" />
			</div>
		)
	}

	return (
		<div className="LogoSelector">
			<input className="fileInput" type="file" onChange={ (event) => handleChange(event) } />
			{previewImage}
		</div>
	);
}

export default LogoSelector;