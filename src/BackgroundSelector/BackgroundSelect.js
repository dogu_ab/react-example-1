import React from 'react';
import './BackgroundSelect.css';

const BackgroundSelect = (props) => {
	return (
		<li onClick={props.click}><img src={props.src} alt="" /></li>
	)
}

export default BackgroundSelect;