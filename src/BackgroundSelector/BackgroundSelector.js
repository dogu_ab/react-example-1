import React from 'react';
import './BackgroundSelector.css';
import image1 from './bg-1.png';
import image2 from './bg-2.png';
import BackgroundSelect from './BackgroundSelect';

const BackgroundSelector = (props) => {
	const BackgroundSelectData = [
		image1,
		image2
	];

	const nextStep = (event) => {
		props.updateStep({step:2});
		props.updateBackground({background: event.target.src})
	}

	const BackgroundSelectList = BackgroundSelectData.map((value, index) => {
		return (
			<BackgroundSelect click={nextStep} key={index} src={value} />
		)
	});

	return (
		<ul className="BackgroundSelector">
			{BackgroundSelectList}
		</ul>
	);
}

export default BackgroundSelector;