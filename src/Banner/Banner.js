import React, { useEffect } from 'react';
import './Banner.css';

const Banner = (props) => {
	const generateText = (firstTime) => {
		var canvas = document.getElementById("myCanvas");
	    var context = canvas.getContext("2d");
	    var imageObj = new Image();

	    imageObj.onload = function(){
	        context.canvas.width = imageObj.width;
  			context.canvas.height = imageObj.height;
  			context.drawImage(imageObj, 0, 0);
	        context.font = "600 32px 'Montserrat'";
	        context.textAlign = "center";
	        context.fillStyle = "#5466fd";
	        context.fillText("AllianceBlock and DIA enter Strategic Partnership", 600, 250);
	        context.fillText("to Deliver DeFi Data Solutions", 600, 294);

	        var base_image = new Image();
			base_image.src = URL.createObjectURL(props.fileArray.file);
			base_image.onload = function() {
				base_image.width = (60 * base_image.width / base_image.height);
				base_image.height = 60;
				context.filter = 'brightness(0) invert(1)';
				context.textAlign = "center";
				context.drawImage(base_image, 960 - base_image.width, 430, (60 * base_image.width / base_image.height), 60);
			}

	        if(firstTime) {
	        	setTimeout(generateText, 100);
	        }
	    };

	    imageObj.src = props.backgroundArray.background;
	}
	useEffect(() => {
	    generateText(true)
	});

	return (
		<div className="Banner">
			<canvas id="myCanvas" width="400" height="200"></canvas>
		</div>
	);
}

export default Banner;