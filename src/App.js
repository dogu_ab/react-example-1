import React, { useState } from 'react';
import './App.css';
import BackgroundSelector from './BackgroundSelector/BackgroundSelector';
import LogoSelector from './LogoSelector/LogoSelector';
import Banner from './Banner/Banner';

const App = props => {
	const [ stateArray, updateStateArray ] = useState({
		step: 1
	});

	const [ backgroundArray, updateBackgroundArray ] = useState({
		background: ''
	});

	const [ fileArray, updateFileArray ] = useState({
		file: ''
	});

	const GenerateBanner = () => {
		console.log(stateArray, backgroundArray, fileArray);
		updateStateArray({step: 4});
	}

	let BackgroundSelectorContainer = null;
	if(stateArray.step === 1) {
		BackgroundSelectorContainer = (
			<div>
				<h3 className="subTitle">Choose Background</h3>
				<BackgroundSelector backgroundArray={backgroundArray} updateBackground={updateBackgroundArray} stateArray={stateArray} updateStep={updateStateArray} />
			</div>
		)
	}

	let LogoSelectorContainer = null;
	if(stateArray.step >= 2 && stateArray.step < 4) {
		LogoSelectorContainer = (
			<div>
				<h3 className="subTitle">Choose Partner Logo</h3>
				<LogoSelector stateArray={stateArray} updateStep={updateStateArray} fileArray={fileArray} updateFile={updateFileArray} />
			</div>
		)
	}

	let BackButton = null;
	if(stateArray.step > 1) {
		BackButton = (
			<div>
				<hr />
				<button className="backButton" onClick={() => updateStateArray({step:stateArray.step - 1})}>Back</button>
			</div>
		)
	}

	let GenerateButton = null;
	if(stateArray.step === 3 && stateArray.step < 4) {
		GenerateButton = (
			<div>
				<button className="generateButton" onClick={GenerateBanner}>Generate Banner</button>
			</div>
		)
	}

	let BannerContainer = null;
	if(stateArray.step === 4) {
		BannerContainer = (
			<div>
				<Banner backgroundArray={backgroundArray} fileArray={fileArray} />
			</div>
		)
	}

	return (
		<div className="App">
			<h1 className="appTitle">Partnership Banner Generator</h1>
			<p className="currentStep">Current step: {stateArray.step}</p>
			<hr />
			{BackgroundSelectorContainer}
			{LogoSelectorContainer}
			{BannerContainer}
			{GenerateButton}
			{BackButton}
		</div>
	);
}

export default App;
