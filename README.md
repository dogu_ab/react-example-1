# My First React App Example

This project is created with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm i`

Install dependencies.

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.